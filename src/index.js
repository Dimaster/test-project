import { gsap } from "./js/green-sock/umd/gsap";
import { CSSRulePlugin } from "./js/green-sock/umd/CSSRulePlugin";
import { ScrollTrigger } from "./js/green-sock/umd/ScrollTrigger";

gsap.registerPlugin(CSSRulePlugin, ScrollTrigger);
gsap.defaults({ease: "none", duration: 3})

const insetContWidth = document.querySelector('.b2-wrap').offsetWidth + "px"
let videoSection = document.querySelector('.video');
videoSection.style.setProperty('--video-width', insetContWidth)

const tl = gsap.timeline();
tl.to(videoSection, { "--video-width": document.documentElement.scrollWidth + "px" })
	.to(videoSection, { "--video-width": document.documentElement.scrollWidth + "px" })
	.to(videoSection, { "--video-width": document.documentElement.scrollWidth + "px" })
	.to(videoSection, { "--video-width": insetContWidth })

ScrollTrigger.create({
	animation: tl,
	trigger: ".video-src",
	start: "-10% 90%",
	end: "110% 10%",
	scrub: 0.3,
	toggleActions: "restart pause reverse pause",
	id: "start",
	onEnter: () => play()
});

function play(){
	document.querySelector('.video-src').play()
}

/*
Label
 */
gsap.to('.naasson', 1, { autoAlpha: 1})

import './styles/index.scss'
